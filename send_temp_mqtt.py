import sys
import config
import os
import getopt
import logging
import paho.mqtt.client as mqtt

msg_format = '%(asctime)s %(levelname)s %(message)s'
date_format= '%Y-%d-%m %H:%M:%S'
logging.basicConfig(format=msg_format, datefmt=date_format, level=logging.INFO)
logger = logging.getLogger(__name__)


def main(argv):
    short_options = 'hV'
    long_options = ['help', 'version']
    options_help = """ [options]

General Options:
   -h   --help          Display this message.
   -V   --version       Display version information.
"""
    try:
        opts, args = getopt.getopt(argv, shortopts=short_options, longopts=long_options)
    except getopt.GetoptError:
        print('Usage: python ' + os.path.basename(sys.argv[0]) + options_help)
        sys.exit(2)
    # if len(opts) < 1:
    #     print(config.APP_NAME, config.APP_VERSION)
    #     print('Usage: python ' + os.path.basename(sys.argv[0]) + options_help)
    #     exit()
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print(config.APP_NAME, config.APP_VERSION)
            print(config.APP_DESCRIPTION)
            print('Use: python ' + os.path.basename(sys.argv[0]) + options_help)
            exit()
        if opt in ('-V', '--version'):
            print(config.APP_NAME, config.APP_VERSION)
            exit()

    send_temp()


def send_temp(topic=config.MQTT_TOPIC,
              host=config.MQTT_HOST,
              port=config.MQTT_PORT,
              keepalive=config.MQTT_KEEPALIVE):
    c_temp = get_cpu_temperature()
    sendMQTT(c_temp, topic, host, port, keepalive)


# Return CPU temperature as a character string
def get_cpu_temperature():
    logging.info('Reading CPU/GPU temp for %s', config.LOCALHOST)

    res = os.popen('vcgencmd measure_temp').readline()
    c_temp = res.replace("temp=", "").replace("'C\n", "")
    logging.info('    CPU/GPU temp:  %s oC', c_temp)

    return c_temp


# MQTT Publisher
def sendMQTT(temp, topic, host, port, keepalive):
    logging.info('Sending MQTT message...')
    logging.info('    MQTT Host:Port %s:%s', host, port)
    client = mqtt.Client()
    client.connect(host, int(port), keepalive)
    client.publish(topic, temp)
    logging.info('    MQTT Topic: %s %s', topic, temp)


if __name__ == "__main__":
   main(sys.argv[1:])

