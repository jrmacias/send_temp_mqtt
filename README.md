# Send Temp MQTT
Send Raspberry Pi CPU/GPU temperature (oC) through MQTT

 - Instalation:
```
    git clone https://jrmacias@bitbucket.org/jrmacias/send_temp_mqtt.git
    cd send_temp_mqtt/
    virtualenv -p python3 venv
    source venv/bin/activate
    pip install -r requirements.txt
```
- Check installation:
```
    python -V
    python send_temp_mqtt.py
```

- Check config.py and update hostname accordingly:

- Add script to cron to be run every minute
```
   crontab -e

   # send CPU/GPU temp regularly
   * * * * * /home/user/<path_to_script>/venv/bin/python /home/user/<path_to_script>/send_temp_mqtt.py >/home/user/<path_to_script>/send_temp_mqtt.log 2>&1
```

i.e.:
```
   * * * * * /home/jrm/bin/send_temp_mqtt/venv/bin/python /home/jrm/bin/send_temp_mqtt/send_temp_mqtt.py >/home/jrm/bin/send_temp_mqtt/send_temp_mqtt.log 2>&1
```
