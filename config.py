# General app settings
APP_NAME = "send_temp_mqtt"
APP_DESCRIPTION = "Send CPU/GPU temp (oC) to the specified MQTT broker"
APP_VERSION = "0.1.0"
# Increment when the WS app changes. Follow the Semantic Versioning schema:
#   MAJOR version when backwards incompatible changes are introduced
#   MINOR version when new functionality is added in a backwards-compatible manner
#   PATCH version when bugs are fixed (but still backwards-compatible)

LOCALHOST = 'raspi31'
MQTT_TOPIC = 'stat/' + LOCALHOST + '/temp/cpu'
MQTT_HOST = '192.168.1.231'
MQTT_PORT = 1883
MQTT_KEEPALIVE = 60

